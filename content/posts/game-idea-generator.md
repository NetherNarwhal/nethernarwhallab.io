---
title: "Game Idea Generator"
description: "Recommends a type of game to create."
color: "#838487"
date: 2020-03-23
tags: ["game","generator"]
---
Thought I would throw together a quick game idea generator based on an article I had been reading last night. As a challenge to myself I may pick a few and see what game I can come up with.

{{< rawhtml >}}
<style>
    .ig-wrapper { width: 80%; border: solid 1px var(--color-border); border-radius: 4px; padding: 1em; margin: 1em auto; }
    .ig-grid { display: grid; grid-template-columns: 50% 50%; grid-column-gap: 1em; grid-row-gap: 0; }
    .ig-icon { width: 1rem; fill: currentColor; display: inline; vertical-align: bottom; }
    .ig-label { position:relative; grid-column: 1 / 2; justify-self: flex-end; }
    .ig-out { grid-column: 2/3; text-decoration: underline dotted var(--color-border); justify-self: flex-start; }
    .ig-out:hover { color: var(--color-text-hover); }
    .ig-tooltip { display: none; background: var(--color-bg-post); color: var(--color-text); margin: 0 2em; padding: 0.5em; position: absolute; z-index: 1; border: solid 1px var(--color-border); border-radius: 4px; box-shadow: 0 1px 5px rgba(0,0,0,0.1); }
    :hover>.ig-tooltip { display: block; }
    .ig-btn { background: var(--color-text-title); color: var(--color-text-tag); text-align: center; vertical-align: middle; font-weight: bold; padding: 2px 1em; margin-top: 1rem; border-radius: 4px; cursor: pointer; user-select: none; }
    .ig-btn:hover { opacity: 0.8; }
</style>

<div class="ig-wrapper">
<div class="ig-grid">
    <div class="ig-label"><svg xmlns="http://www.w3.org/2000/svg" class="ig-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M2.165,19.551C2.351,19.831,2.664,20,3,20h15c0.4,0,0.762-0.238,0.919-0.606l3-7c0.133-0.309,0.101-0.663-0.084-0.944 C21.649,11.169,21.336,11,21,11h-1V7c0-1.103-0.897-2-2-2h-6.1L9.616,3.213C9.44,3.075,9.224,3,9,3H4C2.897,3,2,3.897,2,5v14 h0.007C2.007,19.192,2.056,19.385,2.165,19.551z M17.341,18H4.517l2.143-5h12.824L17.341,18z M18,7v4H6 c-0.4,0-0.762,0.238-0.919,0.606L4,14.129V7h7.556H12H18z"/></svg> Genre:</div>
    <span id="ig-genre" class="ig-out">???<span id="ig-genre-tt" class="ig-tooltip">???</span></span>

    <div class="ig-label"><svg xmlns="http://www.w3.org/2000/svg" class="ig-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M12,2C6.579,2,2,6.579,2,12s4.579,10,10,10s10-4.579,10-10S17.421,2,12,2z M14.113,15H9.986l-1.723-3.094L10.202,9h3.736 l1.871,3.062L14.113,15z M4,12c0-0.953,0.186-1.87,0.508-2.727L7.696,15H4.61C4.22,14.068,4,13.055,4,12z M16.283,9h3.106 C19.78,9.932,20,10.945,20,12c0,0.844-0.143,1.66-0.397,2.432L16.283,9z M18.188,7h-6.653l1.905-2.857 C15.326,4.502,17.002,5.546,18.188,7z M11.093,4.059L9.132,7H9v0.197L7.17,9.942L5.65,7.214C6.95,5.511,8.899,4.319,11.093,4.059z M5.812,17h7.147l-1.709,2.961C9.084,19.748,7.141,18.63,5.812,17z M13.64,19.82l3.357-5.815l1.544,2.526 C17.387,18.173,15.64,19.385,13.64,19.82z"/></svg> Perspective:</div>
    <span id="ig-perspective" class="ig-out">?1?<span id="ig-perspective-tt" class="ig-tooltip">???</span></span>

    <div class="ig-label"><svg xmlns="http://www.w3.org/2000/svg" class="ig-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M12,2C6.486,2,2,6.486,2,12s4.486,10,10,10s10-4.486,10-10S17.514,2,12,2z M12,20c-4.411,0-8-3.589-8-8s3.589-8,8-8 s8,3.589,8,8S16.411,20,12,20z"/><path d="M13 7L11 7 11 12.414 14.293 15.707 15.707 14.293 13 11.586z"/></svg> Interactivity:</div>
    <span id="ig-interactivity" class="ig-out">???<span id="ig-interactivity-tt" class="ig-tooltip">???</span></span>

    <div class="ig-label"><svg xmlns="http://www.w3.org/2000/svg" class="ig-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M12,2C6.579,2,2,6.579,2,12s4.579,10,10,10s10-4.579,10-10S17.421,2,12,2z M12,7c1.727,0,3,1.272,3,3s-1.273,3-3,3 c-1.726,0-3-1.272-3-3S10.274,7,12,7z M6.894,16.772c0.897-1.32,2.393-2.2,4.106-2.2h2c1.714,0,3.209,0.88,4.106,2.2 C15.828,18.14,14.015,19,12,19S8.172,18.14,6.894,16.772z"/></svg> Participants:</div>
    <span id="ig-participants" class="ig-out">???<span id="ig-participants-tt" class="ig-tooltip">???</span></span>

    <div class="ig-label"><svg xmlns="http://www.w3.org/2000/svg" class="ig-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M8.111,21.889c1.602,0,3.108-0.624,4.242-1.757l7.778-7.778c2.339-2.34,2.339-6.146,0-8.485 c-1.134-1.133-2.641-1.757-4.243-1.757c-1.602,0-3.108,0.624-4.242,1.757l-7.778,7.778c-2.339,2.34-2.339,6.146,0,8.485 C5.002,21.265,6.509,21.889,8.111,21.889z M5.282,13.061l7.778-7.778c0.756-0.755,1.76-1.171,2.828-1.171 c1.069,0,2.073,0.416,2.829,1.171c1.559,1.56,1.559,4.098,0,5.657l-7.778,7.778c-0.756,0.755-1.76,1.171-2.828,1.171 c-1.069,0-2.073-0.416-2.829-1.171C3.724,17.158,3.724,14.62,5.282,13.061z"/><circle cx="9" cy="12" r="1"/><circle cx="15" cy="12" r="1"/><circle cx="12" cy="15" r="1"/><circle cx="12" cy="9" r="1"/></svg> Challenge:</div>
    <span id="ig-challenge" class="ig-out">???<span id="ig-challenge-tt" class="ig-tooltip">???</span></span>

    <div class="ig-label"><svg xmlns="http://www.w3.org/2000/svg" class="ig-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M12,2C6.486,2,2,6.486,2,12s4.486,10,10,10s10-4.486,10-10S17.514,2,12,2z M4,12c0-0.899,0.156-1.762,0.431-2.569L6,11l2,2 v2l2,2l1,1v1.931C7.061,19.436,4,16.072,4,12z M18.33,16.873C17.677,16.347,16.687,16,16,16v-1c0-1.104-0.896-2-2-2h-4v-2v-1 c1.104,0,2-0.896,2-2V7h1c1.104,0,2-0.896,2-2V4.589C17.928,5.778,20,8.65,20,12C20,13.835,19.373,15.522,18.33,16.873z"/></svg> World:</div>
    <span id="ig-world" class="ig-out">???<span id="ig-world-tt" class="ig-tooltip">???</span></span>

    <div class="ig-label"><svg xmlns="http://www.w3.org/2000/svg" class="ig-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M19,2H9C7.897,2,7,2.897,7,4v5.586l-4.707,4.707c-0.286,0.286-0.372,0.716-0.217,1.09S2.596,16,3,16v1v4 c0,0.553,0.448,1,1,1h8h8c0.553,0,1-0.447,1-1V4C21,2.897,20.103,2,19,2z M11,20H5v-3v-2v-0.586l3-3l3,3V15v3V20z M19,20h-6v-2v-2 c0.404,0,0.77-0.243,0.924-0.617c0.155-0.374,0.069-0.804-0.217-1.09L9,9.586V4h10V20z"/><path d="M11 6H13V8H11zM15 6H17V8H15zM15 10.031H17V12H15zM15 14H17V16H15zM7 15H9V17H7z"/></svg> World Generation:</div>
    <span id="ig-world-generation" class="ig-out">???<span id="ig-world-generation-tt" class="ig-tooltip">???</span></span>

    <div class="ig-label"><svg xmlns="http://www.w3.org/2000/svg" class="ig-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M11 4L10.5 3 10 4 9 4.125 9.834 4.833 9.5 6 10.5 5.334 11.5 6 11.166 4.833 12 4.125zM19.334 14.666L18.5 13 17.666 14.666 16 14.875 17.389 16.056 16.834 18 18.5 16.889 20.166 18 19.611 16.056 21 14.875zM6.667 6.333L6 5 5.333 6.333 4 6.5 5.111 7.444 4.667 9 6 8.111 7.333 9 6.889 7.444 8 6.5zM3.414 17c0 .534.208 1.036.586 1.414L5.586 20c.378.378.88.586 1.414.586S8.036 20.378 8.414 20L20 8.414c.378-.378.586-.88.586-1.414S20.378 5.964 20 5.586L18.414 4c-.756-.756-2.072-.756-2.828 0L4 15.586C3.622 15.964 3.414 16.466 3.414 17zM17 5.414L18.586 7 15 10.586 13.414 9 17 5.414z"/></svg> Realism:</div>
    <span id="ig-realism" class="ig-out">???<span id="ig-realism-tt" class="ig-tooltip">???</span></span>

    <div class="ig-label"><svg xmlns="http://www.w3.org/2000/svg" class="ig-icon" width="24" height="24" viewBox="0 0 24 24"><path d="M7 11H9V13H7zM7 15H9V17H7zM11 11H13V13H11zM11 15H13V17H11zM15 11H17V13H15zM15 15H17V17H15z"/><path d="M5,22h14c1.103,0,2-0.897,2-2V8V6c0-1.103-0.897-2-2-2h-2V2h-2v2H9V2H7v2H5C3.897,4,3,4.897,3,6v2v12 C3,21.103,3.897,22,5,22z M19,8l0.001,12H5V8H19z"/></svg> Time:</div>
    <span id="ig-time" class="ig-out">???<span id="ig-time-tt" class="ig-tooltip">???</span></span>
</div>
    <div class="ig-btn" onclick="generateGameIdea()">Generate Another</div>
</div>

<script>
    "use strict";
    Array.prototype.random = function() { return this[ Math.floor(Math.random() * this.length) ]; }
    function writeGameIdeaToElement(id, value, tooltip) {
        let el = document.querySelector("#ig-" + id);
        el.innerHTML = value + `<span id="ig-${id}-tt" class="ig-tooltip">${tooltip}</span>`;
    }
    function generateGameIdea() {
        let genres = [{val: "Action", tt: "Games are known for their constant action and focus on the player's physical dexterity. Subgenres include Shooter, Platformer, Fighting, Beat 'em up/Hack and slash, Stealth, Survival/Battle Royale, Rhythm, Survival horror, Metroidvania, Legend of Zelda, Breakout, etc.", link: "https://en.wikipedia.org/wiki/Action_game"}, {val: "Adventure", tt: "Games known for their focus on the story and short puzzles rather than reflex or dexterity based challenges. Subgenres include Text adventures, Graphic adventures, Visual Novels, Walking Simulators, etc.", link: "https://en.wikipedia.org/wiki/Adventure_game"}, {val: "Role-playing", tt: "Games known for taking on the persona and/or a sense of progression over time of the protagonist. Subgenres include Action RPG, Tactical RPG, Sandbox/Open World RPG, JRPG, First Person Party based, Roguelike, Deck builder, MUD/MMORPG, etc", link: "https://en.wikipedia.org/wiki/Role-playing_video_game"}, {val: "Simulation", tt: "Games that seek to recreate experiences of reality or some approximation of it. Subgenres include Management, Construction, Life, Vehicle, Exercise, etc.", link: "https://en.wikipedia.org/wiki/Simulation_video_game"}, {val: "Strategy", tt: "Competitive games that require careful planning and thinking. Subgenres include 4X [explore, expand, exploit, exterminate], RTS, RTT, TBS, TBT, Tower Defense, Auto Battler, Artillery, Poker, Chess, Checkers, etc", link: "https://en.wikipedia.org/wiki/Strategy_video_game"}, {val: "Sports", tt: "Highly competitive games that are based on physical dexterity. Subgenres include Racing, Competitive/Party, Team Sports, etc.", link: "https://en.wikipedia.org/wiki/Sports_game"}, {val: "Puzzle", tt: "Games that focus on knowledge or reasoning. Subgenres include Trivia and Logic games, such as Matching, Sokoban/Minesweeper, Programming, etc", link: "https://en.wikipedia.org/wiki/Puzzle_video_game"}, {val: "Idle", tt: "Games that typically focus on completing a single task or focus solely on social interaction and often require very little involvement from the player. Subgenres include Idle, Casual, MMO, etc.", link: "https://en.wikipedia.org/wiki/List_of_video_game_genres#Idle_gaming"}, {val: "Crossover", tt: "This game is a mixture of one or more genres or subgenres."}];
        let genre = genres.random();
        writeGameIdeaToElement("genre", genre.val, genre.tt);

        let perspective = [{val: "Top Down", tt: "The player views the game world directly from above or sometimes slightly askew, such as an isometric 2.5D view, as they would a board game. In same cases the player may move around the camera freely, while in others it is constrained, such as in a vertical scroller.", link: "https://en.wikipedia.org/wiki/Video_game_graphics#Top-down_perspective"}, {val: "Side-scroller", tt: "The player sees the world from a side-view camera angle, and as the player's character moves left or right, the screen scrolls with them.", link: "https://en.wikipedia.org/wiki/Side-scrolling_video_game"}, {val: "Pseudo-3D", tt: "While mostly a side-scroller there is some depth allowing the player to move back and forth between 'lanes'. Double Dragon, Golden Axe, and Little Big Planet are examples of this style."}, {val: "First Person", tt: "The player sees everything through the eyes of the protagonist."}, {val: "Third Person", tt: "The player views the protagonist from slightly behind and above."}, {val: "Text", tt: "Most gameplay takes place through text rather than graphics."}].random();
        writeGameIdeaToElement("perspective", perspective.val, perspective.tt);

        let interactivity = [{val: "Turn based", tt: "Game flow is partitioned into defined parts, called turns, moves, or plays. A player of a turn-based game is allowed a period of analysis (sometimes bounded, sometimes unbounded) before committing to a game action, ensuring a separation between the game flow and the thinking process. Once every player has taken their turn, that round of play is over, and any special shared processing is done. This is followed by the next round of play.", link: "https://en.wikipedia.org/wiki/Turns,_rounds_and_time-keeping_systems_in_games#Turn-based"}, {val: "Real time", tt: "Game time progresses continuously according to the game clock. There is no separation between game flow and player analysis. A real-time game may be allowed ot be paused, but the player is typically restricted from gain additional information during that time.", link: "https://en.wikipedia.org/wiki/Turns,_rounds_and_time-keeping_systems_in_games#Real-time"}].random();
        writeGameIdeaToElement("interactivity", interactivity.val, interactivity.tt);

        let participants = [{val: "Single", tt: "The player plays without any direct interaction with any other human player. There may be an online aspect to this that involves other players, but their actions must be indirect (ex. online leader boards, short visits from the 'ghosts' of other players, a bones file left from another player that is not player controlled, etc)."}, {val: "Multi-player", tt: "The player will be playing with and/or against other human players."}].random();
        writeGameIdeaToElement("participants", participants.val, participants.tt);

        let challenge;
        if (participants.val === "Multi-player") {
            challenge = [{val: "Co-op", tt: "The players should cooperate in completing the challenges presented by the environment."}, {val: "PvP", tt: "The players will compete against one another with the environment being merely a backdrop."}, {val: "Teams", tt: "The players will be grouped into teams which must then compete against the other teams."}, {val: "Asymmetrical", tt: "This challenge pits either a single player or a team with a small number of players against a team comprised of all of the other players. Typically the player is given extra powers or attributes to make up for the imbalance in numbers."}, {val: "PvA", tt: "It is every person for themselves, however they need to worry about a lot more than just the other players."}, {val: "None", tt: "There are no challenges to the player from either other players or the environments. This is a sandbox to have fun, explore, and socially interact with the other players."}].random();
        } else {
            challenge = [{val: "PvE", tt: "The player must overcome the challenges presented by the environment."}, {val: "None", tt: "There are no challenges to the player from the environment. This is a sandbox to have fun and explore."}].random();
        }
        writeGameIdeaToElement("challenge", challenge.val, challenge.tt);

        let world = [{val: "Linear", tt: "The game world is comprised of predefined levels or layouts with a planned route through it, although the layout may change during the course of play in known ways (wall or building is destructible, etc)." }, {val: "Open", tt: "The world is open so that the player can go down any path they choose at any time. There may be some restrictions placed around a given area based on order of events implemented through items or dialog, however these may not be used excessively." }].random();
        writeGameIdeaToElement("world", world.val, world.tt);

        let worldGen = [{val: "Pre-determined", tt: "The structure of the game world is created by the developers either manually or through a curation process. Each player of the game will experience the same world or the same set of known variations, although there may be a large number of them."}, {val: "Generated", tt: "The structure of the world is generated algorithmically either as the game begins or as it progresses through a process known as procedural generation. Each player and play through will typically be a brand new level bound only by the limitations of the generator.", link: "https://en.wikipedia.org/wiki/Procedural_generation"}].random();
        writeGameIdeaToElement("world-generation", worldGen.val, worldGen.tt);

        let realism = [{val: "Realistic", tt: "There is no magic or magic-like powers within this game world, although some characters may believe there are. There may be alternative history or different realities at play that may seem fantastic compared to the time period, however they will all be grounded in the physics of our known reality."}, {val: "Low Fantasy", tt: "Magic and magic-like powers are real, however are rare and/or highly constrained. Magic would typically be treated as we treat paranormal events or explanations.", link: "https://en.wikipedia.org/wiki/Low_fantasy"}, {val: "High Fantasy", tt: "Magic is real, widely available, and frequently used. It often becomes intrinsic the culture and trade of the inhabitants and shapes the nature of the world.", link: "https://en.wikipedia.org/wiki/High_fantasy"}].random();
        writeGameIdeaToElement("realism", realism.val, realism.tt);

        let time = [{val: "Historic", tt: "The events of the game happen in a world of primitive technology, typically before electrical power. This does not mean that the events or the world must follow those that happened in our reality on Earth."}, {val: "Contemporary", tt: "The events and world leverage the equivalent of the known technology of the present, however it may not follow the events of Earth precisely or even be on Earth."}, {val: "Futuristic", tt: "The events of the world, or worlds, happen in a future with technology or powers that are not available or possible today. 'Any sufficiently advanced technology is indistinguishable from magic.' - Arthur C Clarke"}].random();
        writeGameIdeaToElement("time", time.val, time.tt);
    }
    generateGameIdea();
</script>
{{< /rawhtml >}}
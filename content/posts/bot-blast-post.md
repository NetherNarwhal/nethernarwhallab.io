---
title: "Bot Blast Game"
description: "Code a bot's AI and turn it loose in a battle arena."
color: "#AD5D5D"
date: 2020-03-20
lastmod: 2022-05-22
tags: ["game", "ai"]
---
![BotBlast Game](../../img/botblast.jpg)

Bot Blast is web (javascript) based version of the [Mage Blast](https://gitlab.com/jake133/mage-blast) game. It allows you to battle bots, that you code the AI of, against one another. It is basically the same battle logic and spells as Mage Blast with the addition of a fireball (area of effect), a heal spell, and slight tweaking of things like mana bonuses. When starting the game for the first time you will be given a set of 10 bots with randomly selected names and AIs, which you may modify, add, or remove. The defaults can be customized in the code via the `defaultAIs` variable.

It runs entirely in the browser with only a single dependency. If you want to run the game on your own machine or site you will need to download the `acorn_interpreter.js` file, from the [JS-Interpreter](https://github.com/NeilFraser/JS-Interpreter) project, in the same directory as `botblast.html`.

[Bot Blast Live Demo](/botblast/index.html)

## AI Sandbox
The bot's AI is run within their own sandbox in order to control what they have access to. Since the game was meant as a friendly coding activity there weren't any concerns of someone trying to take over the server, however competition can get fierce and people could have been tempted to gather more information than their bot really should have had access to if a simple `eval` was used. Running a Javascript sandbox inside Javascript is a bit of a niche use case so there wasn't a lot out there. Thankfully, I came across [JS-Interpreter](https://github.com/NeilFraser/JS-Interpreter) and it seemed to fit. It only supports JavaScript syntax up to ES5, but getting it running is pretty straight forward.

1. Pass in the code you want using a new instance of the `Interpreter` class.
2. As part of the initialization, provide an initialization object that maps into the sandbox the objects and functions that you want to expose.

Here is a sample of that logic from the game. There are two functions that do something similar. The validate function, shown below, is to test that the code is parsable during editing. It uses mocked up data to test the field access and has better exception handling. The execute function is used to run the bot's AI during the actual battle.
```javascript
function validateScript(code) {
    // We have to create a new instance each time. Calling run on existing one just picks up at the last line.
    // Create some sample data here to test out their logic.
    let all = [
        { "id": 0, "x": 5, "y": 8, "health": 10, "mana": 3, "hasShield": true },
        { "id": 1, "x": 7, "y": 5, "health": 5, "mana": 10, "hasShield": false },
        { "name": "Test Bot", "id": 2, "x": 10, "y": 4, "health": 8, "mana": 5, "hasShield": false },
    ];
    let bot = all[2]; // Make the 3rd bot the bot they care about for this test.
    let me = bot;
    let others = all.filter(item => item !== bot);
    let arena = {...ARENA_BOUNDS};
    let result = {};

    let jsvm;
    try {
        jsvm = new Interpreter(code, function (interpreter, scope) {
            // Expose some global variables and functions.
            interpreter.setProperty(scope, 'me', interpreter.nativeToPseudo(me));
            interpreter.setProperty(scope, 'others', interpreter.nativeToPseudo(others));
            interpreter.setProperty(scope, 'arena', interpreter.nativeToPseudo(arena));
            interpreter.setProperty(scope, 'log', interpreter.createNativeFunction(
                function(text) { console.log(bot.name + ":", arguments.length ? text : ""); } ));
            interpreter.setProperty(scope, 'castSpell', interpreter.createNativeFunction(
                function(spellName, target, y) { return setSpellCommand(result, all, others, bot, spellName, target, y); } ));
            interpreter.setProperty(scope, 'castRandomSpell', interpreter.createNativeFunction(
                function() { castRandomSpell(result, bot, all, others); } ));
            interpreter.setProperty(scope, 'moveTo', interpreter.createNativeFunction(
                function(x, y) { return setMoveToCommand(result, bot, x, y); } ));
            interpreter.setProperty(scope, 'getDistance', interpreter.createNativeFunction(getDistance)); // No wrapper needed on this one.
            interpreter.setProperty(scope, 'getLowestHealthEnemy', interpreter.createNativeFunction(
                function() { return getLowestHealthEnemy(others); } ));
            interpreter.setProperty(scope, 'getClosestEnemy', interpreter.createNativeFunction(
                function() { return getClosestEnemy(others); } ));
            interpreter.setProperty(scope, 'getRandomEnemy', interpreter.createNativeFunction(
                function() { return Math.floor(Math.random() * others.length); } ));
            interpreter.setProperty(scope, 'isWithinRange', interpreter.createNativeFunction(
                function(target, spellName) { return isWithinRange(bot, target, spellName, all, others); } ));
            interpreter.setProperty(scope, 'haveManaFor', interpreter.createNativeFunction(
                function(spellName) { return haveManaFor(bot, spellName); } ));
        });
    } catch (e) {
        // Should only need to worry about catching parse errors when they are editing them before saving.
        let msg = "Parse Error: " + e.message;
        console.error(msg);
        return msg;
    }

    try {
        jsvm.run();
    } catch (e) {
        let msg = "Runtime error:" + e.message;
        console.error(msg);
        return msg;
    }

    console.log("Results:", result);
}
```
It all seems to work pretty well. The one minor issue is the `Interpreter` instance can't be reused once it has run, so the code must be re-parsed each time. On a relatively new machine with only 10 bots that isn't much of a problem, but 30 bots each turn was 500ms which caused serious stuttering. The `Interpreter` instance could be cloned each time to speed things up, however this example code does not do that. Of course, the data mapped into it during the initialization each turn would also need to be reset. Hopefully, that could be accomplished by manipulating the globalScope attribute. Anyway, it ran fast enough for what I was doing.

If you are interesting in learning more about JS parsing, check out the following:
* [Acorn](https://github.com/acornjs/acorn)
* [Build a JS Interpreter in JavaScript Using Acorn as a Parser](https://blog.bitsrc.io/build-a-js-interpreter-in-javascript-using-acorn-as-a-parser-5487bb53390c)

## Rendering
This was really more a proof of concept, so I didn't invest much time in making it look nice. It uses a standard html canvas with some circles. The only interesting parts were getting the name and status indicators lined up correctly and implementing a few basic animations.

## UI
The screens are hard-coded for the most part as a set of hidden `divs`. When showing a "screen" simply hide all of the divs and make the desired one visible. Not ideal with some occasional repaint flickering, but works well enough that it isn't really an issue in practice. One annoying part is when showing the screens the state of the screen must be reset. For nearly all of the screens this actually is pretty easy. It is mainly the edit screen and the buttons of the battle screen that need to be messed with. Wrapping the display of a screen in a function took care of any updates there. Luckily, there really aren't any places that need to be updated, but something like [Vue](https://vuejs.org/) could easily be hooked in, if needed.

## Issues and Future Refactoring
I consider the game to be feature complete and functional at this point, but there are a few issues and changes I potentially would make if I came back to this.
* Saving the bots to localStorage doesn't seem to work when I run the game hosted on GitLab in both Chrome and Firefox. Others don't seem to have this trouble, so it is likely due to my extensions or some other privacy settings. I haven't looked into. If you intend to put a lot of time into your bot's AI you may want to check whether saving the bot list between site visits works for you or not.
* I think the timer for the bot's think time starts 1 frame before the movement of bots and missiles ends. Need to confirm there isn't a timing issue there and, if so, fix it likely by making the think time slightly longer. Either way I don't think this is noticeable as the game is running.
* The "missiles" or spells work off calculating a destination target location, even if provided an angle or person. It would be simpler to just store the angle and time-to-live (ttl).
* Play around with caching the `Interpreter` instance for each bot to see if we can avoid re-parsing each turn.
* I create new objects each time for the missiles and animations. An object pool approach could be used to limit the garbage collection. JS is pretty good at cleaning up old objects though.
* I'd like to port this to 3D using [Three.js](https://threejs.org/), even if the bots and arena remain simple primitive shapes.
* The light/dark theme toggle doesn't persist across different visits (i.e. not saved to localStorage).
* Implement teams for the bots, like hunger games. You pick enemies from the other team (I assume) until they are eliminated and then you turn on each other. This would help spread out the targeting so 1 bot doesn't get every spell thrown at them which can happen if the AIs are targeting the bot with the lowest health.
* Obstacles would be cool, as would falling off the edge of the arena being deadly. However, introducing pathfinding would greatly complicate the bot's AI logic that a person would need to code, especially if they haven't had experience using pathfinding before.
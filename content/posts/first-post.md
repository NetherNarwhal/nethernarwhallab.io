---
title: "First Post"
description: "Finally got around to starting a site with Gitlab using the Hugo site generator. Here is the process I used."
color: "#4C6A92"
date: 2020-03-19
tags: ["general", "gitlab", "hugo"]
---
I wanted to start a site to track the various things I've been working on, which is basically just an archive of random things that caught my interest at one point or another. I have somewhat of a backlog that I'll try to get out here, but the newer content may lag behind. Anyway, just to get started on some content I'll describe my process for creating this site.

# Gitlab hosting
There are plenty of hosting choices out there from an S3 bucket and Cloudfront on AWS to a dedicated hosting service. I went with Gitlab as I was already planning to upload the code for these projects and it's free. Setting it was pretty easy. Instructions are [here](https://about.gitlab.com/stages-devops-lifecycle/pages/), although I think [these](https://how-to-stuff.gitlab.io/gitlab-pages-how-to/) are more straight forward. I'm going off memory, but here is my summary:
* Create a new project, clone it, and create a public folder and the `.gitlab-ci.yml` file following the examples. You can fork a project off one of the examples, such as the [plain HTML one](https://gitlab.com/pages/plain-html), but depending upon what project you choose you instructions may vary.
* I decided to create a user page rather than a project one. The main difference is the URL you access it with. With a user page you won't have a context root (`<myname>.gitlab.io`), whereas with a project page it will (`<myname>.gitlab.io/<projectname>`). To do that you just need to set the project name AND path (under Advanced section lower down on page) to `<username>.gitlab.io`. The Change path part will look weird as you are typing all of that after the `https://gitlab.com/<username>/` part that is already there, but uneditable. Just ignore that part as it is more for managing the project not the pages site.
* Create and run the pipeline in your project. The shared runners were already setup, so I didn't need to do anything there.
* Make the site public via Settings > Pages. 

After that anytime you git push to that project it should republish.

# Hugo
After looking around at the various static site generators there seemed to be a lot of adoption of [Hugo](https://gohugo.io/) due to its speed and minimalist nature. Before I get into the rest of this I should probably clarify that I somehow (thanks google) got on a 3 year old doc site (gohugobrasil.netlify.com) where everything was out-of-date and half-broken. I spent far too long there and became very frustrated before figuring out my mistake. The correct site is https://gohugo.io/documentation/. 

Here were the steps I followed, which are outlined in Getting Started on the Hugo site:
1. Installed Hugo, via the snap package (`snap install hugo`).
2. Generated a sample project (`hugo new site quickstart`).
3. Pulled down one of the simplest themes I could find which was [Bare](https://themes.gohugo.io/bare-hugo-theme/). I downloaded the zip, unzipped the main folder, renamed it to `bare`, and dropped it in the `themes` folder of my project. I then added it to the `config.toml` for my project (`echo 'theme = "bare"' >> config.toml`).
4. Created a sample post (`hugo new posts/my-first-post.md`).
5. Started the server (`hugo server -D`), which didn't work. I had a few errors in the console. The bare theme, specifically `layouts/partials/header.html`, referenced a few SVG files that didn't exist as they get pulled in via a submodule which didn't come down in the zip obviously. I didn't want to mess with git at the moment and planned to gut the theme anyway so ripped out the references to the missing SVG files.
6. Started the server again and navigated to the [local dev site](http://localhost:1313/). I could see the homepage and the link to the sample post worked, but nothing else was really there. That was expected.
7. I added some tags to the front matter (top of the MD file with the metadata) of my-first-post.md and the site automatically refreshed showing the tags. Clicking on them took me to a list page for the tag.

Cool, I now had something I could work with.

## Customizing the theme
I could override nearly everything in the theme (see below), but decided it was better just to start clean so I did some housekeeping while making sure everything still worked starting with the css. Bare was using a Sass/Bulma build pipeline and I couldn't find where it publishes the css to disk for the test site so I downloaded a copy from the running site, created a static folder (Bare didn't have one already) and copied it there. Then I deleted the `assets`, `resources` (both for the Sass build pipeline for css), `images` (screenshots for the example theme site), and `exampleSite` (test project for example theme site) directories. Then I cleaned up the files in the root folder, deleting everything but the `theme.toml` file.

I had already mocked up a new site very loosely based on the [Baskerville Wordpress theme](https://andersnoren.se/themes/baskerville/) from Anders Norén, so got to work porting it over into the theme.

### Directory structures
Here is the [directory structure](https://gohugo.io/getting-started/directory-structure/) for a blank project:
* **archetypes** - Comes with 1 default template, but your themes will install their templates in here. This is used to generate new content via the CLI commands (ex. hugo new ...).
* **content** - This is where you put your individual posts (.md files). If you create subdirectories it treats those as different content types (a.k.a. categories or sections).
* **data** - Used to store config or information for data driven UIs, read-only unless you have something externally changing them. I didn't have this need so didn't dive into.
* **layouts** - Place file here to extend or override layouts from the theme in order to create the html pages.
* **static** - Copied over (merged with theme's into site root) to the published site as-is. Put your images, css, javascript, etc files here. No structure is enforced here, but having css, js, and img directories are common.
* **themes** - Used to hold the installed themes. Basically the themes directory structure below is copied into a subdirectory with the theme's name (still need to set config.toml "theme" variable to this folder name).
* **config.toml** - Some general configuration and meta information about your site.

Themes directory structure (as produced by the CLI; it doesn't cover everything):
* **archetypes** - Comes with 1 default template, but this is where you define templates to used to generate new content via the CLI commands (ex. hugo new ...).
* **layouts**
    * **_default**
        * **baseof.html** - The starting point (base template) for all pages, including index.
        * **list.html** - The base file (after baseof) for pages that are lists of other pages, like a tag or category page.
        * **single.html** - The base file (after baseof) for the individual pages, like posts.
    * **partials**
        * **footer.html** - Standard footer section that typically gets pulled in from baseof.html.
        * **head.html** - Standard HTML head section that typically gets pulled in from baseof.html.
        * **header.html** - Standard header section that typically gets pulled in from baseof.html.
    * **shortcodes** - Not included in the standard theme starter, but very useful. This is where you can put custom functions/blocks that can be referenced from within your own markup posts. This allows you to include custom HTML in your post's output and other functionality that markdown doesn't support.
    * **404.html** - Template for the page to show when the user types in a URL for a page that doesn't exist.
    * **index.html** - Template for your homepage (note: this still is based on baseof.html). It will typically define the "main" block, or content block, of the page.
* **static** - Copied over (merged with project's into site root) to the published site as-is. Put your images, css, javascript, etc files here. No structure is enforced here, but having css, js, and img directories are common.
* **LICENSE** - How you are licensing the theme's code. Only really used if you are publishing to the themes site.
* **theme.toml** - Some general configuration and meta information about the theme. Looks to be mainly used for the themes site.

When generating the html of your pages, it first starts with the baseof.html and then pulls in content from the various other templates. Most of the templates, called partials, spell out which file (in `/layouts/partials/` mainly) is getting pulled in but you may notice the block called `main` doesn't say where it is coming from. This is because it varies by the content type (a.k.a. kind). For the starting page of your site it gets this from `/layouts/index.html`. For content pages like your posts it gets this from `/layouts/_default/single.html`. And for any pages showing lists of other pages, like a list of pages with a given tag, it uses `/layouts/_default/list.html`. There are a few special cases, like the `404.html` and `403.html` which allow you to define a custom error page for your site, and the `robots.txt` file.

### File Lookups
I've actually oversimplified the description above. Those are where Hugo grabs those files by default, but you can actually override a given file by placing a file with the same name in a different location. This allows you to override most things from the theme without breaking or changing the theme. The lookup rules can actually get pretty complex, but here is the general order for the html files for the `single.html`. Other files use a similar strategy. Note that `[layout]` value below is optionally defined in the `layout` field in front matter (the metadata at the top) of the MD file. It looks for the desired file in the following locations, from top to bottom, and stops when it finds it.
* Look for an `[layout].html` file in local layouts "type" directory, where "type" subdirectory is named same as the `type` field in front matter (the metadata at the top) of the MD file. 
* Look for an `[layout].html` file in local layouts "section" directory, where "section" subdirectory is named the same as subdirectory the MD file is within the content directory.
* Look for an `[layout].html` file in local layouts "_default" directory.
* Repeat the steps above, but looking for a file called "single.html", instead of [layout].html".
* Repeat the steps above (including the 4th), but in the theme's layout directory.

And that is pretty much it as far as the basics go. Now I just needed to change the contents of the various files to be what I wanted. Hugo uses Go templating syntax within the html files. Like everything else with Go, and probably even more so here, it is optimized for parsing at the expensive of read-ability. It isn't particularly intuitive, but I was able to muddle through with cutting and pasting examples. Please consult the [Hugo](https://gohugo.io/templates/introduction/) or [Go](https://golang.org/pkg/text/template/) templating documentation for understanding the syntax and variables.

Once the site is running well, do a local publish (`hugo`) to the `public` directory, and then commit up to git. It will only publish posts that aren't marked as draft, use `-D` if you want to include those or remove the flag from those posts. Next step may be to incorporate Hugo into the Gitlab build pipeline using one of the Hugo examples.

Make sure you add a favicon to the site at the root of your public folder. I used [Favicon Generator](https://realfavicongenerator.net/) to generate everything from a SVG I created.
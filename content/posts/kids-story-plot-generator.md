---
title: "Kids Story Plot Generator"
description: "Generates short plot summaries for kids' stories."
color: "#AF9483"
date: 2020-03-24
tags: ["story","generator"]
---
Testing out [Text Resolver](https://gitlab.com/NetherNarwhal/text-resolver) by making my daughter a story plot generator.

{{< rawhtml >}}
<link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
<script src="/js/resolver.js"></script>
<style>
    .s-content { width: 90%; background: #e2d5c2; padding: 3%; font-family: 'Courgette', sans-serif; color: #6a3708; margin: 0 auto; border-radius: 4px; display: flex; flex-flow: column nowrap; align-items: center; justify-content: space-between;}
    #story { width: 100%; text-align:center; font-size: 1.4em;}
    #story::first-letter { font-size: 1.4em; line-height: 0.7em; font-weight: bold;}
    .s-btn { background: #c16744; color: #fff; text-align: center; vertical-align: middle; font-weight: bold; padding: 0.5em 1em; margin-top: 2rem; border-radius: 4px; cursor: pointer; user-select: none; }
    .s-btn:hover { background: #EEC897; }
</style>

<div class="s-content">
    <div id="story"></div>
    <div class="s-btn" onClick="createPlot()">Tell Another Story</div>
</div>

<script>
"use strict";
Array.prototype.random = function() { return this[ Math.floor(Math.random() * this.length) ]; }
class Character {
    constructor (name, gender, species) {
        this.name = name;
        this.gender = gender;
        this.species = species;
        // Use the actual word rather than the term for it as it is easier to remember when typing. ex. {hero.they}
        // I use the plural form for this attribute name because it is unambiguous (male repeats "his", female repeats "her").
        this.they = (gender === "male") ? "he" : "she";
        this.them = (gender === "male") ? "him" : "her";
        this.themselves = (gender === "male") ? "himself" : "herself";
        this.their = (gender === "male") ? "his" : "her";
        this.theirs = (gender === "male") ? "his" : "hers";
    }
    // This allows us to print out their name without needing to say character.name.
    toString() {
        if (!this.speciesUsed) {
            this.speciesUsed = true; // Only return the full "title" once.
            return this.name + " the " + this.species;
        }
        return this.name;
    }
}

class Party {
    constructor() {
        let rnd = Math.random();
        this.size = (rnd < 0.6) ? 1 : (rnd < 0.9) ? 2 : 3;
        this.they = (this.size === 1) ? "{hero.they}" : "they";
        this.them = (this.size === 1) ? "{hero.them}" : "them";
        this.themselves = (this.size === 1) ? "{hero.themselves}" : "themselves";
        this.their = (this.size === 1) ? "{hero.their}" : "their";
        this.theirs = (this.size === 1) ? "{hero.theirs}" : "theirs";
    }
    toString() {
        switch (this.size) {
            case 2: return "{hero} and {friend1}";
            case 3: return "{hero}, {friend1}, and {friend2}";
            default: return "{hero}";
        }
    }
}

class Context {
    constructor() {
        this.names = {}; // Keeps track of which names have been used so we don't duplicate.

        // Character definitions use creatures, so must define these first.
        this.goodCreature = ["panda","unicorn","mermaid","bunny","baby bunny","mouse","fairy","dragon","puppy","kitten","kangaroo","platypus","hippo","bird","polar bear","penguin","turtle","owl","dog","dolphin","snow tiger","pony","bird","fish","hamster","giraffe",];
        this.badCreature = ["snake","alligator","crocodile","dragon","wolf","lion","spider","slug","squid","octopus","shark","hyena","vulture","buzzard","tiger","leech","jackal","rat","weasel",];
        this.creature = "[{goodCreature}|{badCreature}]";
    
        this.hero = this.getUniqueCharacter();
        this.friend1 = this.getUniqueCharacter();
        this.friend2 = this.getUniqueCharacter();
        this.npc1 = this.getUniqueCharacter();
        this.npc2 = this.getUniqueCharacter();
        this.villain = this.getUniqueCharacter(false);
        this.henchman = this.getUniqueCharacter(false);
        this.party = new Party();
        this.food = [
            "cake","ice cream","cookies","bread","pie","pancakes","waffles","eggs","butter","milk","hamburger","french fries","grilled cheese","spaghetti","macaroni and cheese","cheese","pizza","muffin","cupcake","suckers","popsicle","sandwich","banana","apples","berries","unicorn poop","brownies","egg shells","sausage","pepperoni","cherries","chilli",
        ];
        this.color = [
            "amber","auburn","azure","beige","black","blue","brindle","crimson","ebony","emerald","golden","gray","green","grey","hazel","indigo","ivory","lilac","maroon","ochre","orange","pale","purple","prismatic","red","scarlet","silver","silvery","tawny","umber","violet","white","yellow","mottled","blotted","dapple","tartan","plaid","speckled",
        ];
        this.condition = [
            "amazing","ancient","arcane","broken","charming","cold","cursed","dark","dreaded","elegant","enchanted","forbidden","forgotten","forlorn","forsaken","frozen","glimmering","grand","grim","haunted","hidden","hollow","icy","invisible","knightly","lively","lucky","magical","malevolent","mysterious","nefarious","old","ominous","ornate","pale","party","pristine","royal","ruined","rusted","rusty","sacred","shadowy","shattered","shiny","silent","sinister","{size}","splintered","stone","sturdy","tattered","timeworn","torn","treacherous","twisted","unique","veiled","weathered","wicked","wild","wonderful","wondrous","wooden","worn",
        ];
        this.tree = ["alder","apple","ash","aspen","beech","birch","cedar","cherry","chestnut","cypress","elm","fir","hawthorn","hemlock","hickory","juniper","locust","maple","mulberry","oak","pine","spruce","sycamore","walnut","willow","yew",];
        this.flower = ["azalea","clover","daisy","heather","hibiscus","holly","iris","lilac","lily","lotus","rose","tulip","violet",];
        this.stone = ["alabaster","amber","clay","coral","crystal","diamond","emerald","flint","glass","granite","jade","marble","obsidian","onyx","opal","quartz","ruby","sapphire","slate","stone",];
        this.time = ["dawn","dusk","evening","midnight","morning","night","nightfall","noon","sundown","sunrise","sunset","twilight",];
        this.meal = ["breakfast","brunch","dinner","lunch","snack time","supper",];
        this.volume = ["pint","gallon","cup","teaspoon","tablespoon","quart","ounce","pinch","drop",];
        this.big = ["big","colossal","gargantuan","giant","gigantic","huge","humongous","immense","jumbo","large","mammoth","massive"];
        this.small = ["diminutive","little","mini","miniature","small","tiny","teensy tiny",];
        this.size = ["{big}","{small}"];
        this.seasons = ["spring","summer","autumn","fall","winter",];
        this.instrument = ["bell","chime","drum","fiddle","flute","guitar","harp","horn","lute","piano",];
        this.container = ["backpack","bag","barrel","basket","bottle","box","case","chalice","chest","crate","cup","flask","glass","goblet","jar","mug","pouch","sack","satchel","trunk","urn","vase",];
        this.jewelry = ["amulet","beads","bracelet","brooch","charm","circlet","coin","crown","diamond","emerald","gem","gemstone","medallion","necklace","pearl","pendant","ring","ruby","sapphire","signet","talisman","tiara","topaz",];
        this.stone = ["alabaster","amber","clay","coral","crystal","diamond","emerald","flint","glass","granite","jade","marble","obsidian","onyx","opal","quartz","ruby","sapphire","slate","stone"];
        this.metal = ["adamantine","brass","bronze","cobalt","copper","gold","iron","lead","mithril","nickel","pewter","platinum","silver","steel",];
        this.clothes = ["boots","coat","dress","hat","pants","robe","scarf","shirt","shoes","slippers",];
        this.shapes = ["circle","crescent","cross","hexagon","line","pentagram","point","rectangle","square","star","triangle"];
        this.cloth = ["burlap","cotton","leather","silk","wool"];
        this.location = "[%20 {color}|%20 {condition}|%20 {size}] [castle|house|field|{tree} tree|ruins|city|town|workshop|forest|lake|stream|mountains|ocean|beach|bus stop|library|town hall|stage|tire swing|treehouse|[{food}|{instrument}] shop]";
        this.item = ["balloon","banner","beads","berry","book","brazier","cauldron","candle","candle stick","card","carpet","chain","cog","compass","doll","dust","elixir","figurine","{flower}","fork","fossil","hook","hourglass","ink","key","lamp","lantern","letter","mirror","needle","orb","potion","pottery","powder","quill","ribbon","rock","root","shell","shovel","spoon","statue","stone","sword","thimble","torch","totem","vial","wand","{flower} pedal","{tree} leaf",];
        this.numberWord = ["one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve"];
        this.quality =["long","short","strong","colorful","smart","loud"];

        this.full = [
            "{party} {must} [get|bring {food}] to the {location} before {time} [%50 or the [[surprise|birthday|] [party|event|gathering]] is ruined] [%20 . {extra}]",
            "{party} {must} make a [potion|pie|stew|dessert|meal] with {recipe} before [{party.they} can enter the big show|{meal}]",
            "{party} spend a night at {npc1}'s home. {npc1} shares a bedtime story about a knight named {npc2.name}, who tried to tame {villain} but failed. They hear a noise in the attic, but when they investigate, all they find is {npc1}'s cat, {name}. Later, as {hero} goes to sleep, {hero.they} [dreams about|sees] {npc2.name}'s ghost, who requests {hero.their} help in finding {villain}",
            "While searching the {spot:{location}}{spot} for pieces for {hero.their} [artwork|sculpture|invention], {hero} finds a odd [%20 {size}] [%20 {color}] [%20 {condition}] {thing:{item}}{thing}. {hero} soon begins to experience bad luck and suspects it is the cause. No matter how {hero} gets rid of it, the {thing} somehow always returns. The bad luck finally ends when {hero} returns it to the {spot}",
            // A very clever mouse takes a stroll through the deep dark forest. He encounters many dangerous animals along the way, so to scare them off he decides to invent a story about a frightening creature named the Gruffalo. Imagine his surprise when he meets a real Gruffalo!
            // the jungle animals are having a ball down by the river. The ducks are splashing, the elephants are stomping, the monkeys are chitter-chattering, and the bears are dancing. All this commotion wakes up the crocodile who comes looking for his dinner. Luckily, the clever animals manage to outsmart him!
            "{npc1} is super {quality} and {quality}. {npc1.their.c} ability to [help|assist|comfort|teach|entertain|protect] others makes {npc1.them} a real [asset|hero|favorite] to [everyone|the townsfolk|all the others]. But when {npc1} gets [taken|kidnapped|captured] by the [evil|villainess|cunning|scheming] {villain}, it is up to {party} to save {npc1.them}", // FIXME: work or works?
            // A friendly witch and her cat are blissfully flying through the night sky on a broomstick. The wind unexpectedly blows the witch’s hat away, then her bow, followed by her wand! Fortunately, three kind animals find the witch’s belongings and end up taking a ride on her broomstick. But, the witch’s broom snaps in half under the weight of all the new passengers, and they face the danger of being eaten by a hungry dragon!
            "Two thieves, {villain} and {henchman} are planning to [%25 [capture a {creature} from the zoo|hijack a [train|bus|ship]] and use it to] steal the [Queen|King]'s {item}. But they don’t know that [the crime-busting {party} is on holiday in the same city and|{party}] overheard their cunning plan. With the help of {party.their} friends, {party.they} {come} up with an idea [that will ensure the robbers don’t get away with it|to stop them]", // FIXME: {party} is
            "{party} go into a cave to escape the [cold and rainy weather|swirling snow], but little do they know a {monster:{badCreature}}{monster} is fast asleep in there. When they accidentally wake it up, the cranky {monster} chases the group out. {party} must think of a way to cheer the {monster} up so it will let them back in the warm, dry cave", // FIXME: go at the start
            "{hero} is happily waiting for {hero.their} {food} to bake when a {size} {color} monster appears. {hero.they.c} feels scared and begins to cry, but soon realises that it’s only {hero.their} friend {friend1} dressed up in a monster costume and this makes {hero.them} feel better. {friend1} gives the costume to {hero}, so {hero.they} can have a turn being the monster and they spend the rest of the day playing together",
            "{hero} and {friend1} find a {goal:{item}}{goal} in the {spot:{location}}{spot}. They try the {goal} on, and it looks good on both of them. But there is only one {goal}, and there are two of them. There is only one thing to do. They have to leave the {goal} in the {spot} and forget they ever found it. But the shifting eyes of one of them tells us it’s not quite that easy",
            // Harry is a white dog with black spots, who detests having baths. So one day, he steals the bath brush, buries it in the yard and runs away from home. By the time he returns home, he is so dirty that he looks like a black dog with spots - and his family don’t even recognize him!
            // A tiny snail longs to see the world and hitches a ride on a whale’s tail. Together the snail and the whale go on an incredible adventure, seeing all sorts of amazing sights. But when the whale becomes beached in the bay, it’s up to snail to save the day.
            "{party} embark on an exciting [adventure|journey] to [find|capture] a {monster:{badCreature}}{monster}. On {party.their} journey, {party.they} encounter many different [obstacles|terrains|barriers|challenges]. {party.they.c} slide down a grassy slope, splash through a deep cold river, sludge through thick oozy mud, stumble through a deep dark forest, and walk through a swirling snowstorm. {party.they.c} eventually reach a narrow, gloomy cave where {party.they} {come} face to face with a {monster}, who chases {party.them} all the way back home", // FIXME: tense of the action verbs (slide, splash, sludge, etc).
            "[%50 Dressed in {party.their} [%25 finest] [pirate|sailor] [costumes|outfits], ] {party} set out across the {numberWord} seas in search of treasure. Things take a turn for the worst when {party.their} rubber dingy unexpectedly gets [sunk|shipwrecked|attacked]! But, this leads {party.them} on an adventure that will ultimately be worth much more than gold",
            "When [Grandma|Grandpa|Aunt {name}|Uncle {name}] tells {hero.name} there is a {monster:{badCreature}}{monster} in the garden, {hero.name} doesn’t believe it. We all know that {monster.s} live in the [wild|wilderness|city], not in people’s gardens. But {hero.name} decides to have a look anyway. To {hero.their} surprise, {hero.they} sees dragonflies as big as birds, carnivorous plants, a polar bear who likes fishing and, would you believe it, a friendly {monster}!",
            // It’s time for Morris Mole to go home after a long day of working at Gordon Ratzy’s restaurant. But there’s only one problem... he can’t find his glasses anywhere! Morris decides that he knows the way well enough to find his home without them. Poor Morris pops up in all kinds of homes to unsuspecting families. He eventually finds his home, through the aid of smell (of delicious worm noodles), only to be told that his glasses were on his head all along!
            // Rosie has lost her little baby chick. She looks everywhere - under the hen house, in the basket, behind the wheelbarrow and even through the straw. But Rosie just can’t find her baby chick! And what Rosie doesn’t know is that someone is following her every move… be careful Rosie!
            // Dot and her mum go on an outing to the market, but in all the excitement, Dot accidentally forgets her precious teddy on the train. A kind man at the station assures Dot that Teddy took the train to go on an exciting adventure. Dot's imagination runs wild with all the exciting activities Teddy may be getting up to. Luckily, Teddy finds his way home just in time to get tucked safely into bed.
            // Pip has a brand new balloon, and he decides to show it off by taking a walk around town. Things take a turn for the worst when he lets go of the balloon by accident. It flies into a nearby tree and pops! Pip is devastated, but luckily his best friend Posy knows just how to cheer him up.
            // Odd Dog Out follows a dachshund that just doesn’t quite fit in with all the other dachshunds in her city. All she wants to do is fit in, so she goes on a very long journey to find a place where she will be just like everyone else. After months of walking, she finally discovers the place she’s been looking for. But, she soon realises that being who you are is the best way to be.
            // A little boy named Freddie rescues Bessie-Belle the fairy and is delighted when she offers to grant him his every wish. But Freddie gets frustrated when none of his wishes turns out as expected. Freddie asks for a pet, and receives a net; he wishes for a dog, and she conjures up a frog; and when Freddie requests a parrot, Bessie-Belle gives him a carrot! Luckily, the Fairy Queen arrives to identify the problem: Bessie-Belle is hard of hearing and can’t understand Freddie because he is mumbling. 
        ];
        this.prefix = [
            "Beyond the woods, the seas, and high mountains ",
            "Far beyond the edge of our world, ",
            "In [some|a] [little|distant|old|{condition}] [kingdom|land|village|town], ",
            "It is [told|said] that ",
        ];
        this.start = [
            "{party} {must} [fight|find evidence against|capture|stop|keep an eye on] {villain}",
            "{party} {must} prevent the [escape|release] of {villain}",
            "{party} {must} [reveal|uncover|unmask] {villain}'s plans",
            "{party} {must} [reach|meet {npc1} at|keep watch over|get away from|destroy] the {location}",
            "{party} {must} [learn the history|discover the secrets] of the {location}",
            "{party} {have} [awoken|woken up|found {party.themselves}] in the {location} and {must} find a way out",
            "{party} {must} [escape from|journey to] the {location} in order to recover the {item}",
            "{party} {must} defend the [{item}|{location}] from {villain}",
            "{party} {must} [take|carry|transport|rush|bring|bring back|deliver] the {item} to [{npc1}|the {location}]",
            "{party} {must} [find|search for|seek|locate|retrieve|destroy|scatter the pieces of|repair] the {item}",
            "{party} {must} [help|convince] {npc1} to find [{villain}|the {item}]",
            "{party} {must} [take|get|buy] the {item} from {npc1}",
            "{party} {must} [save|rescue|defend|protect|locate|capture|arrest|ask the advice of|fulfill a request from|do what is asked by] {npc1}",
            "{npc1} [asks|demands|tells|commands] {party} to [take|carry|bring|deliver] the {item} to [{npc2}|the {location}]",
            "{party} {must} warn {npc1} about the {item}",
            "{party} {must} stop {villain} from finding the {item}",
            "{party} {must} learn [gymnastics|how to read|how to cook|basketball|football|soccer|how to count]",
        ];
        this.without = [
            "[{villain}|{npc1}] [getting there first|getting in {party.their} way|finding out|ruining everything|stealing the credit|learning who they are]",
            "{npc1} [helping|being endangered|being taken|tagging along|betraying them|informing {npc2}]",
            "violating {party.their} sworn oath",
            "revealing {party.their} true identity",
            "help from {party.their} friends",
            "using any of {party.their} {food}",
            "stopping to rest",
            "the use of magic",
            "telling anyone {party.their} name",
            "delay",
            "anyone finding out",
        ];
        this.before = [
            "{villain} can complete the [ancient|secret] ritual",
            "{villain} can ruin [the plan|{party.their} plans]",
            "{villain} can [make the next move|stop {party.them}|escape]",
            "{villain} has [a chance to strike|secured the support of {npc1}]",
            "{villain} claims the [throne|crown|title]",
            "[{party.their} family are|{party.their} friends are|{npc1} is] [silenced|killed|arrested|captured|taken]",
            "[{time}|it is too late|time runs out|the equinox]",
            "the [parade|coronation|party|celebration|ball|tournament|competition|trial]",
            "the [snow|storm|{time}] comes",
        ];
        this.or = [
            "{villain} will [ruin the party|ruin everything|destroy the {item}|capture {party.them}|get away|reach the {location} first]",
            "{villain} will have won the [race|bet|competition|trial]",
            "[{party.they}|{npc1}] will lose the [bet|race|competition|challenge|trial]",
            "[{party.they}|{npc1}] will be [upset|exiled|banished|jailed|punished]",
            "[{party.they}|{npc1}] will not be able to [reverse the effects of the spell|lift the curse|receive {party.their} reward]",
            "{party.they} will not have all the clues {party.they} needs to solve the [puzzle|riddle|challenge|trial] [%50 in time]",
            "{npc1} will not have all the clues {npc1.they} needs to solve the puzzle [%50 in time]",
            "a horde of [corrupted|vicious|mutated|evil|vile] [monsters|{creature.s}] will be unleashed",
            "things will be [destroyed|ruined|burned]",
            "there will be [chaos|a great disaster]",
            "the cure will not be found in time",
            "the [storm|blizzard|earthquake|hurricane|tornado|natural disasters] will continue",
        ];
        this.extra = [
            "However, {henchman} has created {a} [ambush|accident|distraction|mess] to [stop|delay] {party.them}",
            "However, it will not be as easy as {hero} expects",
            "[Unexpectedly|Oddly|Strangely|Curiously], {henchman} behaves suspiciously when told of the plan",
            "Unsurprisingly, {npc2} will help, but [for a price|asks for a [%50 {size}] favor in return|only if they help {party.them}|asks to come along]",
            "However, {npc1} learns of the plan and [wants revenge|warns {villain}|wants to help]",
            "Unbeknownst to everyone, {npc1} learns of the plan and [wants revenge|warns {villain}|wants to help|tries to beat {party.them} to it]",
            "Throughout all of this {villain} is doing {villain.their} best to stop {party.them}",
            "[{npc2}|The town|Everyone|The [king|queen|prince|princess]] is counting on them",
        ];
    }
    name(gender, isGood=true) {
        if (!gender) gender = (Math.random() < 0.5 ? "male" : "female");
        let name;
        let nameList;
        if (isGood) {
            if (gender === "female") {
                nameList = ["Daisy","Lexi","Majesty","Princess","Tinkerbell","Diamond","Love","Hearts","Aurora","Cinderella","Belle","Bella","Merida","Mulan","Moana","Ariel","Jasmine","Snow White","Rose Red","Anastasia","Rapunzel","Anna","Elsa","Tiana","Hugs","Candy","Hailey",];
            } else {
                nameList = ["Olaf","Chip","Kristoff","Felix","Flynn","Hudson","Marlin","Wilbur","Basil","Sven",];
            }
        } else {
            if (gender === "female") {
                nameList = ["Maleficent","Cruella","Ursula","Madam Mim","Tremaine","Medusa","Grimhilde","Yzma","Gothel","Trouble","Evil Queen of Doom",];
            } else {
                nameList = ["Darth","Captain Hook","Hades","Jafar","Scar","Gaston","Gaston","Hans","Pete","Slade","Chernabog",];
            }
        }

        let cnt = 0;
        do {
            name = nameList.random();
        } while (this.names[name] && cnt++ < 30);
        this.names[name] = true;
        return name;
    }
    maleName() { return this.name("male"); }
    femaleName() { return this.name("female"); }
    getUniqueCharacter(isGood=true) {
        let gender = (Math.random() < 0.8) ? "female" : "male";
        return new Character(this.name(gender, isGood), gender, (isGood ? this.goodCreature : this.badCreature).random());
    }
    ingredient() {
        let num = Math.floor(Math.random() * 10) + 1;
        let text = num + " " + ((num === 1) ? "[{volume}|{container}]" : "[{volume.s}|{container.s}]");
        return text + " of [%25 {color}|%25 {condition}] {food}";
    }
    recipe() {
        let text = "";
        let loop = Math.floor(Math.random() * 4) + 1;
        for (let i = 0; i < loop; i++) {
            text += this.ingredient() + (loop > 1 ? ", " : " ");
        }
        text += "and " + this.ingredient();
        return text;
    }
    must() { return (!this.party || this.party.size < 2) ? "[must|needs to|has to]" : "[must|need to|have to]"; }
    have() {return (!this.party || this.party.size < 2) ? "has" : "have"; }
    come() { return (!this.party || this.party.size < 2) ? "comes" : "come"; }
}

function createPlot() {
    let resolver = new Resolver();
    let context = new Context();
    let plot = resolver.resolve(context, "[%50 {full}|[%10 {prefix.c}] {start.c} [%60 [ before {before}| without {without}]] [%50 or {or}] [%25 . {extra}]].");
    document.getElementById("story").innerHTML = plot;
}
createPlot();
</script>
{{< /rawhtml >}}<br/>
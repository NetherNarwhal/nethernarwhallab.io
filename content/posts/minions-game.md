---
title: "Minions Game"
description: "Create a one click/touch, physics based minions game."
color: "#5C7148"
date: 2024-12-20
draft: false
tags: ["game", "physics", "matter.js"]
---
This is a 2017 prototype created based on the game [RedBros](https://redbros.fandom.com/wiki/Redbros_Wiki), but it is also similar to the more famous [Pikmin](https://en.wikipedia.org/wiki/Pikmin) and [Overlord](https://en.wikipedia.org/wiki/Overlord_(2007_video_game)) games, where you indirectly control a group of minions by telling them where to go on the map or what to target. Other than that they decide how to best get there and when to take action... kind of.

Here is how it describes the game play:
> It involves leading a group of heroes through themed stages containing monsters and traps. You give orders to your heroes by touching where you would like them to go or attack. They may be split into two groups by using two fingers or put into formations by dragging a single finger, which can lead to more interesting challenges.
>
> Completing stages grants stars, one for each of the 3 objectives accomplished, as well as money or gems. Once you gather enough stars you will increase a level, which can increase the number of heroes starting each stage, attack and health for new heroes, and damage multiplier for your skills.
>
> If you lose all heroes before completing a stage, and opt not to continue it, then you will lose one of 3 hearts. The hearts regenerate over time. You will be unable to play new stages if you do not have any hearts. 

That comes from the linked wiki. Obviously, the prototype is not attempting to do all of that. It is just getting the basics of the game play in place such as moving the minions around, giving them obstacles and traps, and having things for them to attack like monsters and barrels/crates.

[Minions Live Demo](/minions/index.html)

# Movement
While it is certainly possible to manage moving the individual minions yourself, it is easier to use a physics engine instead for this simple use case. At the time I did a scan and selected [Matter.js](https://brm.io/matter-js/) ([source](https://github.com/liabru/matter-js)) as it was generally recommended by the community and was more up-to-date than many of the others.

In an actual game we could add in waypoints and better obstacle avoidance, but for this prototype I kept the logic simple. If the user clicks on a:
* Monster or breakable (ex. barrel, crate, etc): the minions will try to attack it, which means moving in that direction if the target isn't within melee or missile range.
* Location: the minions will try to go directly there, even if that spot isn't passable or is outside of the arena although I have a TODO listed to potentially ignore those.

The target logic is handled in the mouseup event.

```javascript
canvas.addEventListener('mouseup', function(event) {
    event.preventDefault();
    if (event.button !== 0) return;
    updateMousePos(mouse, event);

    // See if they clicked on something specific.
    let body = Query.point(enemies, mouse);
    if (body.length > 0) {
        for (let i = 0; i < heroes.length; i++) {
            heroes[i].target = body[0];
        }
        changeState(STATE_ATTACK);
        createHitEffect(mouse.x, mouse.y, 40, "#FF0000");
        return;
    }
    body = Query.point(breakables, mouse);
    if (body.length > 0) {
        for (let i = 0; i < heroes.length; i++) {
            heroes[i].target = body[0];
        }
        changeState(STATE_ATTACK);
        createHitEffect(mouse.x, mouse.y, 40, "#FF0000");
        return;
    }
    // TODO: check for things to open or activate (plate switch, lever, statue, chest, doorway, portal, etc).

    // Must be a movement location. Set their goal.
    target = new Vec2(mouse.x, mouse.y); // TODO: Do we want to verify it is a reachable destination?
    changeState(STATE_MOVE);
    createHitEffect(mouse.x, mouse.y, 40, "#00BFFF");

    // TODO: at some point we will want to check for drag formation?
});
```

If there are obstacles in the way the minion tries to keep going hoping the physics engine works them around it but otherwise there is no intelligence to their movement. If they get stuck the user will need to click somewhere more in the minion's line of sight to straighten things out. The minion's movement logic each turn is handled in moveEntity. It simply tries to apply force in the direction towards the target.

```javascript
// Move a body by applying force to its body.
function moveEntity(entity, goal, tempVec) {
    tempVec = tempVec || new Vec2(); // Let them pass one in to reuse to cut down on garbage collection.
    tempVec.setFromVec(goal).subtract(entity.position).normalize().scale(entity.movespeed);
    Body.applyForce(entity, entity.position, tempVec);
   
    // TODO: See if they stepped on a switch or mine or should we let collisions handle?
}
```

# Interaction
The game is mainly about how the minions interact with the environment around them, so let's dive into the behaviors of the various objects in the game.

## Minions (a.k.a. Heroes)
As covered above, the minions can interact with their environment. You may notice that after a targeted monster or breakable is destroyed they will keep attacking things around them. However if told to move to a location they will ignore any nearby monsters. We manage this by assigning a state for the group of minions, not individually. They can either be Idle, Moving, or Attacking. When moving they will ignore all monsters and other targets. This may seem strange but it allows the minions to obey if the user wants them to retreat. If we had check for target logic in there they would immediately ignore the move orders and continue the attack, possibly getting themselves or other minions killed.

There are two types of minions. Those with short (i.e. fighters) and long attack ranges (i.e. mages/archers). The minion will keep moving closer to the target until it is within range.

This is handled in `updateHeroes`.

## Enemies
The enemies are also rather simple. They sit idle until one of the minions moves within their attack zone. The attack zone is represented in the physics environment as a sensor created during `init`. A sensor is used to detect the collision with the minion rather than the game needing to see if any minion is within enemy sight range every turn. Once the enemies are activated they will seek the nearest minion and attack. Like the minions, there are short and long range versions of the enemies.

This is handled in `updateEnemies`.

## Projectiles
Minions or enemies with a range attack will shoot projectiles (a.k.a. missiles), which is handled in `createMissile`. The missile instances are actually pooled, so an old missile is reused instead of creating a new one. This reduces the number of objects that Javascript needs to create and dispose of.

Each active missile is moved each turn in `updateMissiles`, however the code to handle collisions with an entity or obstacle is actually created when the missile itself is created in `createMissile`. Regardless of what it hits it will attempt to damage it and then go away.

## Traps
Simple traps are created in the aptly named `createTrap` function. They appear and disappear on a schedule. they are implemented as sensors so they won't imped the movement of any minion, enemy, or missile. When they are active they will cause damage to any entity within them. On the sample environment you can see a large trap in the upper left of the level.

# Limiting Frame Rates
Matter.js, like most physics engines, uses a fixed time step. This means it always assume the same amount of time has passed between updates. Typically this is tuned to something like 60 hz, which is 60 times a second. `window.requestAnimationFrame` will invoke the `main` method at the refresh rate matching the computer's display. While this is often close to 60 hz, it can sometimes be much higher, especially for gaming machines, or slower if processing between updates takes too long. There isn't much that can be done about updating too slowly, but if running too fast it can make the game unplayable. To ensure this is limited the main method keeps track of the last time it ran an update and only allows another if that is more than 1/60 of a second ago (note 1 second is 1000 ms, which is why it is `1000 / 60`).

# Future Plans and Thoughts
Movement control is indirect so the minions are going to do some ridiculous things that wouldn't happen if controlling them individually. Allowing gestures to create formations helps somewhat, but doesn't eliminate this problem. Controlling individual minions turns this into a RTS game, which isn't the goal.

It can also be difficult to differentiate the minions. Most of these style of games shy away for allowing significant customization of individual minions due to how erratic their movement can be. As a result the minions are often interchangeable cogs or at least easily replaceable at certain points. That can make it difficult to scale or provide new challenges as the game goes on. This means the user will see most of the tricks upfront so there isn't a lot to surprise them with later.

All that being said, having a group of minions swarm a tough monster or move in unison across an area with a lot of smaller obstacles is really satisfying. I'll likely come back to this project at some point but there is a reason this genre never really took off. Some tough design trade-offs would be needed to make the game actually challenging that would likely make it less enjoyable.
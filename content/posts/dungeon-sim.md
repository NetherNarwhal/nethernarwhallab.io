---
title: "Management Simulation Game"
description: "Manage a fantasy city. Starter JS game in the management simulation genre."
date: 2020-04-08
draft: true
tags: ["game", "ai"]
---
Let's create a simple management simulation game where you build a fantasy city and hire adventurers who complete quests to earn the town money and also defend the town, all on their own. For design inspiration think about something similar to [Dungeon Village](https://kairosoft.fandom.com/wiki/Dungeon_Village) for the overall feel, but we'll start with something even simpler than that from a logic perspective.

## UI
Let's start with the UI so we have something we can see right away and build upon.

canvas
https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage
https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame
handling resizing
grid
obstacles/buildings
dialogs?

## Pathfinding
AStar, link to Red Blob?

[Pathfinding](https://en.wikipedia.org/wiki/Pathfinding)
[A*](https://en.wikipedia.org/wiki/A*_search_algorithm) and [Red Blob's](https://www.redblobgames.com/pathfinding/a-star/introduction.html)
[Jump Point Searching](https://en.wikipedia.org/wiki/Jump_point_search)
[Near-Optimal Hierarchical Pathfinding (HPA*)](https://webdocs.cs.ualberta.ca/~mmueller/ps/hpastar.pdf)

## AI Behavior
State, Hierarchical State, Behavior Trees, Utility, Goal Oriented

Creatures

Adventurers

## Progression

## Polish
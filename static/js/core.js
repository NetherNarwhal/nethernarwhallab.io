"use strict";
// Helper methods I have found broadly useful over the years across many different projects. These do extend built-in object prototypes which isn't typically a good idea, however since everyone else avoids them and I don't typically use 3rd party frameworks the chances of conflicts are fairly low.
Array.prototype.remove = function(item) { const i = this.indexOf(item); if (i > -1) this.splice(i, 1); } // Assumes there is just 1 of item.
Array.prototype.random = function() { return this[Math.floor(Math.random() * this.length)]; }
Array.prototype.shuffle = function() { // Uses https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
    let i, j, k;
    for (i = this.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        k = this[i]; this[i] = this[j]; this[j] = k;
    }
    return this;
}
Math.clamp = (min, val, max) => { if (val < min) return min; if (val > max) return max; return val; }
String.isString = (s) => (typeof s === "string" || s instanceof String);

// FIXME: $e is for backwards compatibility. Delete when no longer used.
// Create an HTML or SVG element. (optional) props is {}, (optional) children is Strings and/or Elements. Children may be passed in place of props.
// ex: let node = $e("div", { id: "something", style: "background:red;" }, "Text1", $e("input")); // Text1 and <input> will be children of the newly created element.
// To set dataset attributes, pass them as props. ex: {"data-my-attr": "myAttr string value"}
// Some SVG elements cannot be created since they conflict with their html versions (ex. "a"). See https://developer.mozilla.org/en-US/docs/Web/SVG/Element.
// For svg, letting the html parser handle the .html string is sometimes better. Ex. with symbols needing to define the viewBox on the symbol instead of the `use` if using $e.
function $e(tag = "div", props, ...children) { // Children accepts a String for text nodes.
    const el = ($e.svgTags[tag] === true || props?.isSVG === true)
        ? document.createElementNS("http://www.w3.org/2000/svg", tag)
        : document.createElement(tag);
    if (props) {
        if (Array.isArray(props)) el.append(...props); // Is an array of strings and/or nodes, not properties. Assume they want to append.
        else if (props instanceof window.Node || typeof props !== "object") el.append(props); // Is a DOM node and/or primitives, not properties. Assume they want to append.
        else for (const prop in props) if (props[prop] !== undefined) el.setAttribute(prop, props[prop]); // Common props: class, id, name, style, onclick, src, data-x, viewBox,etc.
    }
    if (children.length > 0 && children[0]) {
        if (children.length > 1) el.append(...children);
        else if (Array.isArray(children[0])) el.append(...children[0]);
        else el.append(children[0]);
    }
    return el;
}
$e.svgTags = {animate:true, animateMotion:true, animateTransform:true, circle:true, clipPath:true, defs:true, desc:true, discard:true, ellipse:true, feBlend:true, feColorMatrix:true, feComponentTransfer:true, feComposite:true, feConvolveMatrix:true, feDiffuseLighting:true, feDisplacementMap:true, feDistantLight:true, feDropShadow:true, feFlood:true, feFuncA:true, feFuncB:true, feFuncG:true, feFuncR:true, feGaussianBlur:true, feImage:true, feMerge:true, feMergeNode:true, feMorphology:true, feOffset:true, fePointLight:true, feSpecularLighting:true, feSpotLight:true, feTile:true, feTurbulence:true, filter:true, foreignObject:true, g:true, hatch:true, hatchpath:true, line:true, linearGradient:true, marker:true, mask:true, metadata:true, mpath:true, path:true, pattern:true, polygon:true, polyline:true, radialGradient:true, rect:true, set:true, stop:true, svg:true, switch:true, symbol:true, text:true, textPath:true, tspan:true, use:true, view:true};
// Not supported due to naming conflicts: a, image (alias for img), script, style, title. Pass isSVG:true in props.


window["$"] = (selector) => document.querySelector(selector);
window["$$"] = (selector) => document.querySelectorAll(selector);
$.config = (el, props, ...children) => {
    if (props) {
        if (Array.isArray(props)) el.append(...props); // An array of strings and/or nodes, not properties. Assume they want to append.
        else if (props instanceof window.Node || typeof props !== "object") el.append(props); // A DOM node and/or primitives, not properties. Assume they want to append.
        else for (const prop in props) if (props[prop] !== undefined) el.setAttribute(prop, props[prop]);
        // Common props: class, id, name, style, onclick, src, data-x, viewBox, etc.
    }
    if (children.length > 0 && children[0]) {
        if (children.length > 1) el.append(...children);
        else if (Array.isArray(children[0])) el.append(...children[0]);
        else el.append(children[0]);
    }
    return el;
}

// Create an HTML element. (optional) props is {}, (optional) children is Strings and/or Elements. Children may be passed in place of props. Children accepts a String for text nodes.
// ex: let node = $.div({id: "something", style: "background:red;"}, "Text1", $.span("test")); // Text1 and <span> will be children of the newly created element.
// To set dataset attributes, pass them as props. ex: $.div({"data-my-attr": "myAttr string value"})
$.create = (tag="div", props, ...children) => $.config(document.createElement(tag), props, ...children);
[
    "a", "abbr", "address", "area", "article", "aside", "audio", "b", "base", "bdi", "bdo", "blockquote", "body", "br", "button", "canvas", "caption", "cite", "code", "col", "colgroup", "data", "datalist", "dd", "del", "details", "dfn", "dialog", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hr", "html", "i", "iframe", "img", "input", "ins", "kbd", "label", "legend", "li", "link", "main", "map", "mark", "meta", "meter", "nav", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "picture", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "script", "section", "select", "small", "source", "span", "strong", "style", "sub", "summary", "sup", "table", "tbody", "td", "template", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "u", "ul", "var", "video", "wbr"
].forEach(tag => $[tag] = function (props, ...children) { return $.create(tag, props, ...children); });
// Create a SVG element. Usage: $.svg() OR $.svg.use()
$.svg = (props, ...children) => $.config(document.createElementNS("http://www.w3.org/2000/svg", "svg"), props, ...children);
[
    "a", "animate", "animateMotion", "animateTransform", "circle", "clipPath", "defs", "desc", "discard", "ellipse", "feBlend", "feColorMatrix", "feComponentTransfer", "feComposite", "feConvolveMatrix", "feDiffuseLighting", "feDisplacementMap", "feDistantLight", "feDropShadow", "feFlood", "feFuncA", "feFuncB", "feFuncG", "feFuncR", "feGaussianBlur", "feImage", "feMerge", "feMergeNode", "feMorphology", "feOffset", "fePointLight", "feSpecularLighting", "feSpotLight", "feTile", "feTurbulence", "filter", "foreignObject", "g", "hatch", "hatchpath", "image", "line", "linearGradient", "marker", "mask", "metadata", "mpath", "path", "pattern", "polygon", "polyline", "radialGradient", "rect", "script", "set", "stop", "style", "switch", "symbol", "text", "textPath", "title", "tspan", "use", "view"
].forEach(tag => $.svg[tag] = function (props, ...children) { return $.config(document.createElementNS("http://www.w3.org/2000/svg", tag), props, ...children); });

// Adds some syntactical sugar to the built-in EventTarget. These will be inherited by Element, Document, ShadowRoot, etc.
EventTarget.prototype.on = function(type, listener, options) {
    // Type can be an array. Pass `once: true` in options for one-time listeners.
    if (Array.isArray(type)) for (const t of type) this.addEventListener(t, listener, options);
    else this.addEventListener(type, listener, options); return this;
}
EventTarget.prototype.off = function(type, listener, options) {
    // Type can be an array. Pass same listener & options used with on.
    if (Array.isArray(type)) for (const t of type) this.removeEventListener(t, listener, options);
    else this.removeEventListener(type, listener, options); return this;
}

// Adds some syntactical sugar to the built-in Element (https://developer.mozilla.org/en-US/docs/Web/API/Element) by making the common methods chainable.
Element.prototype.add = function(...nodesOrDOMStrings) { this.append(...nodesOrDOMStrings); return this; }
Element.prototype.empty = function() { this.replaceChildren(); return this; }
Element.prototype.query = function(selector) { return this.querySelector(selector) }
Element.prototype.queryAll = function(selector) { return this.querySelectorAll(selector) }
Element.prototype.addClass = function(...className) { this.classList.add(...className); return this; }
Element.prototype.removeClass = function(...className) { this.classList.remove(...className); return this; }
Element.prototype.is = function(className) { return this.classList.contains(className) }
Element.prototype.getProp = function(p) { let v = getComputedStyle(this).getPropertyValue(p); if (v) v = v.trim(); return v; }
Element.prototype.setProp = function(p, v) { this.style.setProperty(p, v); return this; }
Object.defineProperty(Element.prototype, "html", {get: function() { return this.innerHTML }, set: function(html) { this.innerHTML = html }});
Object.defineProperty(Element.prototype, "text", {get: function() { return this.textContent }, set: function(text) { this.textContent = text }});
// Adds some syntactical sugar to the built-in Document. Document doesn't extend Element so not all extensions apply.
Document.prototype.add = Element.prototype.add;
Document.prototype.query = Element.prototype.query;
Document.prototype.queryAll = Element.prototype.queryAll;
Document.prototype.getProp = function(p) {let v = getComputedStyle(this.documentElement).getPropertyValue(p); if (v) v=v.trim(); return v; }
Document.prototype.setProp = function(p, v) { this.documentElement.style.setProperty(p, v); return this; }
// Adds some syntactical sugar to the built-in DocumentFragment, and therefore ShadowRoot. DocumentFragment doesn't extend Element or Document so not all extensions apply.
DocumentFragment.prototype.add = Element.prototype.add;
DocumentFragment.prototype.query = Element.prototype.query;
DocumentFragment.prototype.queryAll = Element.prototype.queryAll;